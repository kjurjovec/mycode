#!/usr/bin/python3
"""Alta3 Research | <your name here>
   Using an HTTP GET to determine when the ISS will pass over head"""

# python3 -m pip install requests

# For making HTTP requests
import requests

# For performing UNIX Time Conversion
import time

# For accepting arguments from the cmd line
import argparse

# For working with panda formats
import pandas

# Making http request to find ISS future position per Lon and Lat
#POSITION = "http://api.open-notify.org/iss-pass.json?lat=40.59&lon=105.8"

def main():
    
    latitude = ''    
    while latitude == '':
        latitude = input('Latitude = ')

    longitude = ''
    while longitude == '':
        longitude = input('Longitude = ')

    #print(latitude)
    #print(longitude)
 
    # Making http request to find ISS future position per Lon and Lat
    POSITION = "http://api.open-notify.org/iss-pass.json?lat="+latitude+"&lon="+longitude

    #Send HTTP Get request to the API of ISS position
    gotresp = requests.get(POSITION)

    #Decode Response From ISS Position
    gotdecoded = gotresp.json()

    #Practice creating a DataFrame in order to create an Excel Spreadsheet
    gotdecodeddf = pandas.DataFrame(gotdecoded['response'])
    gotdecodeddf.to_excel("wow.xlsx", index=False)

    #Output for when the ISS will be over the specified Location
    print('The ISS will be overhead in Fort Collins during the folling dates and times:')
    for x in gotdecoded["response"]:
        UNIXTime = x.get("risetime")
        timeconvert = time.ctime(UNIXTime)
        print(timeconvert)

if __name__ == "__main__":
    main()

